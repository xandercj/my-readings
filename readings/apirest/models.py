import enum
from werkzeug.security import generate_password_hash, check_password_hash
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, date
from apirest import ma, db

class IdiomaCategoria(str, enum.Enum):
    EN = "Inglés"
    ES = "Español"
    JP = "Japones"
    
class Categoria(str, enum.Enum):
    COL = "Literatura Colombiana"
    UNI = "Literatura Universal"
    COMIC = "Literatura Gráfica y Cómic"
    LATAM = "Literatura Latinoamericana"
    FANTASIA = "Fantasía"
    FICCION = "Ficción"

'''
Modelos
'''
class Usuario(db.Model):
    usuario = db.Column(db.String(200))
    email = db.Column(db.String(100), primary_key = True)
    password = db.Column(db.String(100))
    libros = db.relationship('Libro', backref = 'usuario', lazy = True)

    def hashear_clave(self):
        '''
        Hashea la clave en la base de datos
        '''
        self.password = generate_password_hash(self.password, 'sha256')

    def verificar_clave(self, clave):
        '''
        Verifica la clave hasheada con la del parámetro
        '''
        return check_password_hash(self.password, clave)

class Libro(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String(100))
    autor = db.Column(db.String(100))
    categoria = db.Column(db.Enum(Categoria))
    idioma = db.Column(db.Enum(IdiomaCategoria))
    descripcion = db.Column(db.String(280))
    fecha_publicacion = db.Column(db.DateTime(), default = datetime.now)
    revision = db.Column(db.String(280))
    usuario_libro = db.Column(db.String(100), db.ForeignKey('usuario.email'), nullable = False)

'''
Schemas
'''
class UsuarioSchema(ma.Schema):
    '''
    Representa el schema de un admin
    '''
    class Meta:
        fields = ("usuario", "email", "password")

class LibroSchema(ma.Schema):
    '''
    Representa el schema de un concurso
    '''
    class Meta:
        fields = ("id", "titulo", "autor", "categoria", "idioma", "descripcion", "fecha_publicacion", "revision")

usuario_schema = UsuarioSchema()
libro_schema = LibroSchema()
libros_schema = LibroSchema(many = True)
